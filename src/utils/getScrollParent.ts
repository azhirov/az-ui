export function getScrollParent(element: HTMLElement | null, includeHidden = false) {
  if (!element) return document.scrollingElement || document.documentElement;
  let style = getComputedStyle(element);
  const excludeStaticParent = style.position === 'absolute';
  const overflowRegex = includeHidden ?
    /(auto|scroll|hidden)/ :
    /(auto|scroll)/;

  if (style.position === "fixed") return document.scrollingElement || document.documentElement;
  for (let parent: HTMLElement | null = element; (parent = parent.parentElement);) {
    style = getComputedStyle(parent);
    if (excludeStaticParent && style.position === "static") {
      continue;
    }
    if (overflowRegex.test(style.overflow + style.overflowY + style.overflowX)) return parent;
  }

  return document.scrollingElement || document.documentElement;
}
