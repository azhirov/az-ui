import { UnwrapRef } from 'vue';
export type ExposedToObject<T extends object> = {
  [key in keyof T]: UnwrapRef<T[key]>;
};
