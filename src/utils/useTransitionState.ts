import { ref } from 'vue';

export enum TransitionState {
  ENTERING,
  ENTERED,
  LEAVING,
  LEAVED
}

export function useTransitionState(initialState: TransitionState = TransitionState.LEAVED) {
  const transitionState = ref(initialState)

  return {
    transitionState,
    onBeforeEnterTransition: () => transitionState.value = TransitionState.ENTERING,
    onBeforeLeaveTransition: () => transitionState.value = TransitionState.LEAVING,
    onAfterEnterTransition: () => transitionState.value = TransitionState.ENTERED,
    onAfterLeaveTransition:  () => transitionState.value = TransitionState.LEAVED,
  }
}
