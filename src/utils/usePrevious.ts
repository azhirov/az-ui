import {
  computed,
  ComputedRef,
  ref,
  Ref,
  UnwrapRef,
  watch,
} from 'vue';

export default function usePrevious<T>(valueRef: Ref<UnwrapRef<T>>, initialValue: T): ComputedRef<UnwrapRef<T>> {
  const internal = ref<T>(initialValue)
  watch(valueRef, (newVal, oldVal) => {
    internal.value = oldVal
  })
  return computed(() => internal.value)
}
