import { Directive, DirectiveBinding } from 'vue';
import { ObjectDirective } from '@vue/runtime-core';

export interface TargetElement extends HTMLElement {
  _onClickOutside?: null | ((event: MouseEvent) => void),
}

export type ClickOutsideHandler = (event: MouseEvent) => void

export interface ClickOutsideProps {
  handler: ClickOutsideHandler,
  middleware?: (event: MouseEvent, el: HTMLElement) => boolean,
}

function checkPropsAndAssignHandler(el: TargetElement, binding: DirectiveBinding<ClickOutsideProps | ClickOutsideHandler>) {
  if (!binding.value) return;

  if (typeof binding.value !== "function" && !binding.value?.handler) {
    let warn = `[v-click-outside:] provided expression is not a function or [ClickOutsideProps], but has to be`;
    console.warn(warn);
    return;
  }

  el._onClickOutside = (event: MouseEvent) => {
    if (event.target && el.contains(event.target as Node)) return;
    if (typeof binding.value === 'function') {
      binding.value(event);
      return;
    }
    if (binding.value.middleware && binding.value.middleware(event, el) === false) {
      return;
    }
    binding.value.handler(event);
  }
}

const vClickOutside: ObjectDirective<TargetElement, ClickOutsideProps | ClickOutsideHandler> = {
  mounted(el: TargetElement, binding: DirectiveBinding<ClickOutsideProps | ClickOutsideHandler>, vNode) {
    checkPropsAndAssignHandler(el, binding);
    el._onClickOutside && document.addEventListener('mousedown', el._onClickOutside)
  },
  updated(el: TargetElement, binding: DirectiveBinding<ClickOutsideProps | ClickOutsideHandler>) {
    checkPropsAndAssignHandler(el, binding);
  },
  beforeUnmount(el: TargetElement) {
    el._onClickOutside && document.removeEventListener('mousedown', el._onClickOutside)
    el._onClickOutside = null
  }
}

export default vClickOutside;
