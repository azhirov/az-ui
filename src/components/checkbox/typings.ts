import { ComputedRef, Ref, WritableComputedRef } from 'vue';
import { TPrimitiveNull } from '../radio/typings';

export interface CheckboxGroupProvide {
  actualValue: WritableComputedRef<TPrimitiveNull[]>,
  checkItem: (value: TPrimitiveNull) => void,
  uncheckItem: (value: TPrimitiveNull) => void,
  disabled: Ref<boolean>,
  block: Ref<boolean>,
  name: Ref<string | undefined>,
}
