import { Ref } from 'vue';

export interface PopupExpose {
  updatePosition: () => void,
  el: Ref<HTMLElement | null>,
}
