import { Ref } from 'vue';

export interface Rect {
  left: number
  top: number
  right: number
  bottom: number
  width: number
  height: number
}

export interface Dimensions {
  width: number
  height: number
}

export enum Position {
  'TopLeft' = 'top-left',
  'Top' = 'top',
  'TopRight' = 'top-right',
  'BottomLeft' = 'bottom-left',
  'Bottom' = 'bottom',
  'BottomRight' = 'bottom-right',
  'Left' = 'left',
  'Right' = 'right',
  'Center' = 'center'
}

// Иначе IDE не предлагает варианты при передаче значения в props компонента
export const positions = [
  'top-left',
  'top',
  'top-right',
  'bottom-left',
  'bottom',
  'bottom-right',
  'left',
  'right',
  'center',
] as const

export interface PositionerExpose {
  updatePosition: () => void,
  el: Ref<HTMLElement | undefined>,
}



