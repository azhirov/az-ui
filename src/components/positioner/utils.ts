import { Dimensions, Position, Rect } from './Positioner.types';

function makeRect(
  { width, height }: Dimensions,
  { left, top }: { left: number, top: number }
): Rect {
  const roundedLeft = Math.ceil(left)
  const roundedTop = Math.ceil(top)
  return {
    width,
    height,
    left: roundedLeft,
    top: roundedTop,
    right: roundedLeft + width,
    bottom: roundedTop + height
  }
}

function flipHorizontal(position: Position) {
  switch (position) {
    case Position.TopLeft: return Position.BottomLeft
    case Position.Top:
    default:
      return Position.Bottom
    case Position.TopRight: return Position.BottomRight
    case Position.BottomLeft: return Position.TopLeft
    case Position.Bottom: return Position.Top
    case Position.BottomRight: return Position.TopRight
  }
}

function isAlignedOnTop(position: Position) {
  return [Position.Top, Position.TopLeft, Position.TopRight].includes(position)
}

function isAlignedHorizontal(position: Position) {
  return [Position.Left, Position.Right].includes(position)
}

function isFitsOnBottom(rect: Rect, viewportOffset: number, viewport: Dimensions) {
  return rect.bottom < viewport.height - viewportOffset
}
function isFitsOnTop(rect: Rect, viewportOffset: number) {
  return rect.top > viewportOffset
}
function isFitsOnRight(rect: Rect, viewportOffset: number, viewport: Dimensions) {
  return rect.right < viewport.width - viewportOffset
}
function isFitsOnLeft(rect: Rect, viewportOffset: number) {
  return rect.left > viewportOffset
}


function getTransformOrigin(position: Position, contentDimensions: Dimensions, targetRect: Rect) {
  const centerY = Math.round(targetRect.height / 2)
  if (position === Position.Left) {
    return `${contentDimensions.width}px ${centerY}px`
  }
  if (position === Position.Right) {
    return `0 ${centerY}px`
  }

  const centerX = Math.round(targetRect.width / 2)
  if (isAlignedOnTop(position)) {
    return `${centerX}px ${contentDimensions.height}px`
  }
  return `${centerX}px 0`
}

/**
 * Function that takes in numbers and position and gives the final coords.
 */
function getRect(
  position: Position,
  contentDimensions: Dimensions,
  targetRect: Rect,
  targetOffset: number
) {
  const leftRect = targetRect.left + targetRect.width / 2 - contentDimensions.width / 2
  const alignedTopY = targetRect.top - contentDimensions.height - targetOffset
  const alignedBottomY = targetRect.bottom + targetOffset
  const alignedRightX = targetRect.right - contentDimensions.width
  const alignedLeftRightY = targetRect.top + targetRect.height / 2 - contentDimensions.height / 2

  switch (position) {
    case Position.Left:
      return makeRect(contentDimensions, {
        left: targetRect.left - contentDimensions.width - targetOffset,
        top: alignedLeftRightY
      })
    case Position.Right:
      return makeRect(contentDimensions, {
        left: targetRect.right + targetOffset,
        top: alignedLeftRightY
      })
    case Position.Top:
      return makeRect(contentDimensions, {
        left: leftRect,
        top: alignedTopY
      })
    case Position.TopLeft:
      return makeRect(contentDimensions, {
        left: targetRect.left,
        top: alignedTopY
      })
    case Position.TopRight:
      return makeRect(contentDimensions, {
        left: alignedRightX,
        top: alignedTopY
      })
    default:
    case Position.Bottom:
      return makeRect(contentDimensions, {
        left: leftRect,
        top: alignedBottomY
      })
    case Position.BottomLeft:
      return makeRect(contentDimensions, {
        left: targetRect.left,
        top: alignedBottomY
      })
    case Position.BottomRight:
      return makeRect(contentDimensions, {
        left: alignedRightX,
        top: alignedBottomY
      })
  }
}

function getPosition(
  position: Position,
  contentDimensions: Dimensions,
  targetRect: Rect,
  targetOffset: number,
  viewport: Dimensions,
  viewportOffset: number
): { position: Position, rect: Rect } {
  const isHorizontal = isAlignedHorizontal(position)

  if (isHorizontal) {
    const leftRect = getRect(Position.Left, contentDimensions, targetRect, targetOffset)
    const rightRect = getRect(Position.Right, contentDimensions, targetRect, targetOffset)

    const fitsOnLeft = isFitsOnLeft(leftRect, viewportOffset)
    const fitsOnRight = isFitsOnRight(rightRect, viewportOffset, viewport)

    if (position === Position.Left) {
      if (fitsOnLeft) {
        return {
          position,
          rect: leftRect,
        }
      }
      if (fitsOnRight) {
        return {
          position: Position.Right,
          rect: rightRect
        }
      }
    }

    if (position === Position.Right) {
      if (fitsOnRight) {
        return {
          position,
          rect: rightRect
        }
      }

      if (fitsOnLeft) {
        return {
          position: Position.Left,
          rect: leftRect
        }
      }
    }

    // Default to using the position with the most space
    const spaceRight = Math.abs(viewport.width - viewportOffset - rightRect.right)
    const spaceLeft = Math.abs(leftRect.left - viewportOffset)

    if (spaceRight < spaceLeft) {
      return {
        position: Position.Right,
        rect: rightRect
      }
    }

    return {
      position: Position.Left,
      rect: leftRect
    }
  }

  const positionIsAlignedOnTop = isAlignedOnTop(position)
  let topRect
  let bottomRect

  if (positionIsAlignedOnTop) {
    topRect = getRect(position, contentDimensions, targetRect, targetOffset)
    bottomRect = getRect(flipHorizontal(position), contentDimensions, targetRect, targetOffset)
  } else {
    topRect = getRect(flipHorizontal(position), contentDimensions, targetRect, targetOffset)
    bottomRect = getRect(position, contentDimensions, targetRect, targetOffset)
  }

  const topRectFitsOnTop = isFitsOnTop(topRect, viewportOffset)
  const bottomRectFitsOnBottom = isFitsOnBottom(bottomRect, viewportOffset, viewport)

  if (positionIsAlignedOnTop) {
    if (topRectFitsOnTop) {
      return {
        position,
        rect: topRect
      }
    }

    if (bottomRectFitsOnBottom) {
      return {
        position: flipHorizontal(position),
        rect: bottomRect
      }
    }
  }

  if (!positionIsAlignedOnTop) {
    if (bottomRectFitsOnBottom) {
      return {
        position,
        rect: bottomRect
      }
    }

    if (topRectFitsOnTop) {
      return {
        position: flipHorizontal(position),
        rect: topRect
      }
    }
  }

  // Default to most spacious if there is no fit.
  const spaceBottom = Math.abs(viewport.height - viewportOffset - bottomRect.bottom)

  const spaceTop = Math.abs(topRect.top - viewportOffset)

  if (spaceBottom < spaceTop) {
    return {
      position: positionIsAlignedOnTop ? flipHorizontal(position) : position,
      rect: bottomRect
    }
  }

  return {
    position: positionIsAlignedOnTop ? position : flipHorizontal(position),
    rect: topRect
  }
}


export default function getFittedPosition(
  position: Position,
  contentDimensions: Dimensions,
  targetRect: Rect,
  targetOffset: number,
  viewport: Dimensions,
  viewportOffset: number = 0,
): {
  rect: Rect,
  transformOrigin: string,
  position: Position,
} {
  const { position: finalPosition, rect } = getPosition(
    position,
    contentDimensions,
    targetRect,
    targetOffset,
    viewport,
    viewportOffset
  )

  // Push rect to the right if overflowing on the left side of the viewport.
  if (rect.left < viewportOffset) {
    rect.right += Math.ceil(Math.abs(rect.left - viewportOffset))
    rect.left = Math.ceil(viewportOffset)
  }

  // Push rect to the left if overflowing on the right side of the viewport.
  if (rect.right > viewport.width - viewportOffset) {
    const delta = Math.ceil(rect.right - (viewport.width - viewportOffset))
    rect.left -= delta
    rect.right -= delta
  }

  // Push rect down if overflowing on the top side of the viewport.
  if (rect.top < viewportOffset) {
    rect.top += Math.ceil(Math.abs(rect.top - viewportOffset))
    rect.bottom = Math.ceil(viewportOffset)
  }

  // Push rect up if overflowing on the bottom side of the viewport.
  if (rect.bottom > viewport.height - viewportOffset) {
    const delta = Math.ceil(rect.bottom - (viewport.height - viewportOffset))
    rect.top -= delta
    rect.bottom -= delta
  }

  const transformOrigin = getTransformOrigin(position, contentDimensions, targetRect)

  return {
    rect,
    position: finalPosition,
    transformOrigin
  }
}
