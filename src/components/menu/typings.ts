export interface MenuOption {
  label: string,
  value: any,
  disabled?: boolean
}
