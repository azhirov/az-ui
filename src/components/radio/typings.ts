import { ComputedRef, Ref, WritableComputedRef } from 'vue';

export type TPrimitiveNull = string | number | boolean | null

export interface RadioGroupProvide {
  actualValue: WritableComputedRef<TPrimitiveNull>,
  updateActualValue: (value: TPrimitiveNull) => void,
  disabled: Ref<boolean>,
  block: Ref<boolean>,
  name: ComputedRef<string>,
}
