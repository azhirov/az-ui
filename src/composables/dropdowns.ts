import { nextTick, PropType, Ref, ref } from 'vue';

export function useFocusBlurActivator(
  elRef: Ref<HTMLElement | undefined>,
  popupRef: Ref<HTMLElement | undefined>,
  isDisabledRef: Ref<boolean>,
  emit: (event: 'focus' | 'blur', ...args: any[]) => void
) {
  const isFocused = ref(false)
  const isPopupActive = ref(false)

  const onClick = (evt: MouseEvent) => {
    if (isDisabledRef.value) return;
    isPopupActive.value = !isPopupActive.value
  }
  const onFocus = (evt: FocusEvent) => {
    if (isDisabledRef.value) return;
    isFocused.value = true
    emit('focus', evt)
  }
  const onBlur = (evt: FocusEvent) => {
    if (isDisabledRef.value) return;
    if (evt.relatedTarget !== popupRef.value && !popupRef.value?.contains(evt.relatedTarget as HTMLElement)) {
      isPopupActive.value = false
      isFocused.value = false
      emit('blur', evt)
    }
  }
  const onPopupFocus = (evt: FocusEvent) => {
    console.log('popup focus')
  }

  const onPopupBlur = (evt: FocusEvent) => {

    if (evt.relatedTarget !== elRef.value && !elRef.value?.contains(evt.relatedTarget as HTMLElement)) {
      isPopupActive.value = false
      isFocused.value = false
      emit('blur', evt)
    }
  }

  return {
    onClick,
    isFocused,
    isPopupActive,
    onFocus,
    onBlur,
    onPopupFocus,
    onPopupBlur,
  }
}

export const dropdownOptionsProp = {
  options: {
    type: Array as PropType<Record<string, any>[]>,
    required: true,
  },
  labelKey: {
    type: String,
    default: 'label',
  },
  valueKey: {
    type: String,
    default: 'value',
  },
  disabledKey: {
    type: String,
    default: 'disabled',
  }
}
