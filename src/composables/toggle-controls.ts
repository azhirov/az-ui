import { computed, inject, ref, Ref, toRef, watch } from 'vue';
import { RadioGroupProvide, TPrimitiveNull } from '../components/radio/typings';
import { CheckboxGroupProvide } from '../components/checkbox/typings';
import CheckboxGroup from '../components/checkbox/checkbox-group.vue';

export function injectRadioGroup() {
  return inject<RadioGroupProvide | undefined>('radioGroup', undefined);
}
export function injectCheckboxGroup() {
  return inject<CheckboxGroupProvide | undefined>('checkboxGroup', undefined);
}
export function getComputedName(propName: Ref<string | undefined>, radioGroup?: RadioGroupProvide, checkboxGroup?: CheckboxGroupProvide) {
  return computed<string | undefined>(() => {
    return radioGroup
      ? radioGroup.name.value
      : checkboxGroup
        ? checkboxGroup.name.value
        : propName.value
  })
}

export function getRadioInputValue(
  internalValue: Ref<TPrimitiveNull>,
  radioGroup?: RadioGroupProvide,
) {
  return computed<TPrimitiveNull>(() =>
     radioGroup
      ? radioGroup.actualValue.value
      : internalValue.value
  );
}

export function geCheckboxInputValue(
  internalValue: Ref<TPrimitiveNull>,
) {
  return computed<TPrimitiveNull>(() => internalValue.value);
}

export function useModelValueWatcher<T = TPrimitiveNull>(modelValueRef: Ref<T>, internalValueRef: Ref<T>) {
  watch(modelValueRef, (val) => internalValueRef.value = val)
}

export function getComputedIsBlock(blockPropRef: Ref<boolean>, radioGroup?: RadioGroupProvide, checkboxGroup?: CheckboxGroupProvide) {
  return computed(() => !!(blockPropRef.value || radioGroup?.block.value || checkboxGroup?.block.value))
}
export function getComputedIsDisabled(disabledPropRef: Ref<boolean>, radioGroup?: RadioGroupProvide, checkboxGroup?: CheckboxGroupProvide) {
  return computed(() => !!(disabledPropRef.value || radioGroup?.disabled.value || checkboxGroup?.disabled.value))
}

export function useFocusBlurHook() {
  const isFocused = ref(false)
  const onFocus = () => { isFocused.value = true }
  const onBlur = () => { isFocused.value = false }
  return { isFocused, onFocus, onBlur }
}
