import { computed, getCurrentInstance, ref, Ref } from 'vue';

export function proxyValue<T>(propValue: Ref<T>, emitFn: (event: 'update:modelValue', ...args: unknown[]) => void) {
  const internalValue = ref(propValue.value) as Ref<T>
  const actualValue = computed<T>({
    get(): T {
      return internalValue.value
    },
    set(val: T) {
      internalValue.value = val
      emitFn('update:modelValue', val)
    }
  });
  return { internalValue, actualValue }
}

export function focusable(emitFn: (event: 'focus' | 'blur', ...args: unknown[]) => void) {
  const isFocused = ref(false)
  function onFocus(evt: FocusEvent) {
    isFocused.value = true
    emitFn('focus', evt)
  }

  function onBlur(evt: FocusEvent) {
    isFocused.value = false
    emitFn('blur', evt)
  }

  return {
    isFocused,
    onFocus,
    onBlur,
  }
}

export function inputWithErrorState(errorProp: Ref<string | boolean>) {
  const isError = computed(() => (errorProp.value === '' || !!errorProp.value))
  const errorText = computed(() => typeof errorProp.value === 'string' ? errorProp.value : '')
  return { isError, errorText }
}

export function withLabel(labelProp: Ref<string | null | undefined>) {
  const instance = getCurrentInstance()
  const hasLabel = computed(() => (!!labelProp.value && !!labelProp.value.trim().length) || instance?.slots.label )
  return { hasLabel }
}

export function withDescription(descriptionProp: Ref<string | null | undefined>) {
  const instance = getCurrentInstance()
  const hasDescription = computed(() => (!!descriptionProp.value && !!descriptionProp.value.trim().length) || !!instance?.slots.description )
  return { hasDescription }
}


export const inputProps = {
  base: {
    type: {
      type: String,
      default: 'text'
    },
    modelValue: String,
    disabled: Boolean,
  },
  withPlaceholder: {
    placeholder: String,
  },
  withLabel: {
    label: String,
  },
  withErrorState: {
    error: {
      type: [String, Boolean],
      default: false,
    },
  },
  withDescription: {
    description: String,
  },
  clearable: {
    clearable: Boolean,
  }
}
